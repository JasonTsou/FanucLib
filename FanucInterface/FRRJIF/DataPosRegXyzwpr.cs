using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataPosRegXyzwpr
    {
        internal DataPosRegXyzwpr()
        {
        }

        public FRIF_DATA_TYPE DataType { get; }

        public int Group { get; }

        public DataTable DataTable { get; }

        public bool Valid { get; }

        public bool SetValueXyzwpr(int Index, ref Array Xyzwpr, ref Array Config)
        {
            return true;
        }

        public bool Update()
        {
            return true;
        }

        public int StartIndex { get; }

        public int EndIndex { get; }

        public int ObjectID { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get; }

        public void Reset()
        {
        }

        public bool SetValueXyzwpr2(int Index, float X, float Y, float Z, float W, float P, float R, float E1, float E2, float E3, short C1, short C2, short C3, short C4, short C5, short C6, short C7)
        {
            return true;
        }
    }
}