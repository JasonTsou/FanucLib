using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataNumReg
    {
        internal DataNumReg()
        {
        }

        internal ushort Offset { get; set; }

        //internal ushort

        public FRIF_DATA_TYPE DataType { get; internal set; }

        public int StartIndex { get; internal set; }

        public int EndIndex { get; internal set; }

        public DataTable DataTable { get; internal set; }

        public bool Valid { get; }

        public bool GetValue(int Index, ref object Value)
        {
            return true;
        }

        public bool SetValue(int Index, object Value)
        {
            return true;
        }

        public int ObjectID { get { return 21; } }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get { return Core.DebugLog; } }

        public bool SetValues(int Index, object Value, int Count)
        {
            return true;
        }

        public bool SetValuesInt(int Index, ref int Value, int Count)
        {
            return true;
        }

        public bool SetValuesReal(int Index, ref float Value, int Count)
        {
            return true;
        }
    }
}