using System;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public enum FRIF_DATA_TYPE
    {
        NUMREG_INT,
        NUMREG_REAL,
        POSREG,
        CURPOS,
        TASK,
        SYSVAR_INT,
        SYSVAR_REAL,
        SYSVAR_POS,
        SYSVAR_STRING,
        ALARM_LIST,
        ALARM_CURRENT,
        ALARM_PASSWORD,
        POSREG_XYZWPR,
        STRREG,
        STRREG_COMMENT,
        NUMREG_COMMENT,
        POSREG_COMMENT,
        SDI_COMMENT,
        SDO_COMMENT,
        RDI_COMMENT,
        RDO_COMMENT,
        UI_COMMENT,
        UO_COMMENT,
        SI_COMMENT,
        SO_COMMENT,
        WI_COMMENT,
        WO_COMMENT,
        WSI_COMMENT,
        WSO_COMMENT,
        GI_COMMENT,
        GO_COMMENT,
        AI_COMMENT,
        AO_COMMENT,
        TASK_IGNORE_MACRO,
        TASK_IGNORE_KAREL,
        TASK_IGNORE_MACRO_KAREL
    }
}