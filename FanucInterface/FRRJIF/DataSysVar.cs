using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataSysVar
    {
        internal DataSysVar()
        {
        }

        public FRIF_DATA_TYPE DataType { get; }

        public DataTable DataTable { get; }

        public bool Valid { get; }

        public bool GetValue(ref object Value)
        {
            return true;
        }

        public string SysVarName { get; }

        public bool SetValue(object Value)
        {
            return true;
        }

        public int ObjectID { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get; }
    }
}